#include<stdio.h>

/*The way i did this was i checked for both scenarios,the first when ticket price increases and then for ticket price decreasing and deduced which way i can get the highest profit and what it is*/
/*Also note that in each scenario the program automatically stops when it get the highest profit and wont check anymore*/
int attendance(int ticket){
	int i=0,a=180;
	while(i!=ticket&&a>0)
	{
		i=i+5;
		a=a-20;
	}


	return a;
}

int cost(int ticket){
	return 500+(3*attendance(ticket));
}

int earnings(int ticket){
	return ticket*attendance(ticket);
}

int profit(int ticket){
	return earnings(ticket)-cost(ticket);
}

int main()
{		printf("First lets see for increasing ticket prices \n");
        int p1,t1=15,k;
		p1=profit(t1);
		do{
            printf("The profit at ticket price(%d) is %d \n",t1,p1);
            k=p1;
            t1=t1+5;
            p1=profit(t1);

		}while(p1>k);
		printf("The highest profit when you keep increasing ticket price is %d and the ticket price is %d \n \n",k,(t1-5));
		printf("Now lets see for decreasing ticket prices \n");
		int p2,m,t2;
		t2=15;
		p2=profit(t2);
		do{
            printf("The profit at ticket price(%d) is %d \n",t2,p2);
            m=p2;
            t2=t2-5;
            p2=profit(t2);

		}while(p2>m);
		printf("The highest profit when you keep decreasing ticket price is %d and the ticket price is %d \n \n",m,(t2+5));

		if(p2>p1){
            printf("The highest profit is when you keep on decreasing the ticket price and the ticket price is %d and the profit is %d \n",(t2+5),m);
		}
		else{
            printf("The highest profit is when you keep on increasing the ticket price and the ticket price is %d and the profit is %d \n",(t1-5),k);
		}

		return 0;




	}
